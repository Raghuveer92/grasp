package com.example.neerajyadav.grasp;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Neeraj Yadav on 8/25/2016.
 */
public class RegisterFragment extends Fragment {
    TextView tv_show, tv_r_with_fb, tv_agreement;
    Button btn_register;
    CheckBox cb_lic;
    private int flag =0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment, container, false);

//        tv_show = (TextView) view.findViewById(R.id.tv_show);
        tv_r_with_fb = (TextView) view.findViewById(R.id.tv_r_with_fb);
        tv_agreement = (TextView) view.findViewById(R.id.tv_agreement);
        btn_register = (Button) view.findViewById(R.id.btn_register);
        cb_lic = (CheckBox) view.findViewById(R.id.cb_lic);

        final TextInputLayout input_fname= (TextInputLayout) view.findViewById(R.id.input_layout_fname);
        final TextInputLayout input_email= (TextInputLayout) view.findViewById(R.id.input_layout_email);
        final TextInputLayout input_password= (TextInputLayout) view.findViewById(R.id.input_layout_password);
        final TextInputLayout input_lname= (TextInputLayout) view.findViewById(R.id.input_layout_lname);
        final TextInputLayout input_mnumber= (TextInputLayout) view.findViewById(R.id.input_layout_mnumber);
        final TextInputLayout input_country= (TextInputLayout) view.findViewById(R.id.input_layout_country);



//        tv_show.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (flag==0){
//
//                    input_password.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//                    tv_show.setText("HIDE");
//                    flag =1;
//                }
//                else {
//                    flag =0;
//                    input_password.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD|InputType.TYPE_CLASS_TEXT);
//                    tv_show.setText("SHOW");
//
//                }
//            }
//        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input_email.getEditText().getText().toString().length() == 0){
                    input_email.setError(getString(R.string.cannot_empty));
                    return;
                }
                else {
                    input_email.setError(null);
                }
                 if (!android.util.Patterns.EMAIL_ADDRESS.matcher(input_email.getEditText().getText().toString()).matches()){
                    input_email.setError(getString(R.string.invalid_email));
                     return;
                }
                 else {
                     input_email.setError(null);
                 }
                 if (input_password.getEditText().getText().toString().length()==0){
                    input_password.setError(getString(R.string.cannot_empty));
                     return;
                }
                 else {
                     input_password.setError(null);
                 }
                 if (input_fname.getEditText().getText().toString().length()<=3) {
                    input_fname.setError(getString(R.string.name_error));
                     return;
                }
                 else {
                     input_fname.setError(null);
                 }

                 if (input_mnumber.getEditText().getText().toString().length()<=9 ||input_mnumber.getEditText().getText().toString().length()>10) {
                    input_mnumber.setError(getString(R.string.mnumber));
                     return;
                }
                 else {
                     input_mnumber.setError(null);
                 }
                 if (input_country.getEditText().getText().toString().length()==0) {
                    input_country.setError(getString(R.string.cannot_empty));
                     return;
                }
                 else {
                     input_country.setError(null);
                 }
                 if (!cb_lic.isChecked()){
                    Toast.makeText(getContext(), "Please Accept The License Agreement", Toast.LENGTH_LONG).show();
                     return;
                }
                Intent i =new Intent(getActivity(),ProfileActivity.class);
                startActivity(i);

            }


        });

        return view;
    }

}
