package com.example.neerajyadav.grasp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

/**
 * Created by Neeraj Yadav on 8/25/2016.
 */
public class SignInFragment extends Fragment {
    Button btn_signin;
    CheckBox cb_license;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.signin_fragment, container, false);
        btn_signin = (Button) view.findViewById(R.id.btn_signin);
        cb_license = (CheckBox) view.findViewById(R.id.cb_license);

        final TextInputLayout input_email= (TextInputLayout) view.findViewById(R.id.input_layout_email);
        final TextInputLayout input_password= (TextInputLayout) view.findViewById(R.id.input_layout_password);


        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input_email.getEditText().getText().toString().length() == 0){
                    input_email.setError(getString(R.string.cannot_empty));
                    return;
                }
                else {
                    input_email.setError(null);
                }
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(input_email.getEditText().getText().toString()).matches()){
                    input_email.setError(getString(R.string.invalid_email));
                    return;
                }
                else {
                    input_email.setError(null);
                }
                 if (input_password.getEditText().getText().toString().length()==0){
                    input_password.setError(getString(R.string.cannot_empty));
                     return;
                }
                 else {
                     input_email.setError(null);
                 }
                 if (!cb_license.isChecked()){
                    Toast.makeText(getContext(), "Please Accept The License Agreement", Toast.LENGTH_LONG).show();
                     return;
                }
            }
        });

        return view;
    }
}
