package com.example.neerajyadav.grasp;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        MyPageAdapter myPageAdapter = new MyPageAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(myPageAdapter);

    }
        class MyPageAdapter extends FragmentPagerAdapter {

            public MyPageAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        SignInFragment signInFragment = new SignInFragment();
                        return signInFragment;
                    case 1:
                        RegisterFragment registerFragment = new RegisterFragment();
                        return registerFragment;
                }
                return null;
            }

            @Override
            public int getCount() {
                return 2;
            }


            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "SIGN IN";
                    case 1:
                        return "REGISTER";
                }
                return null;
            }

        }
    }